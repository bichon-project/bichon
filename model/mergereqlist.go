// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package model

import (
	"sort"

	log "github.com/sirupsen/logrus"
)

type MergeReqFilter func(mreq MergeReq) bool

// Return true if a is less than b
type MergeReqSorter func(a, b MergeReq) bool

type MergeReqList struct {
	Unfiltered []*MergeReq
	Active     []*MergeReq
	Filter     MergeReqFilter
	Sorter     MergeReqSorter
}

func (mreqs *MergeReqList) Insert(mreq *MergeReq) {
	found := false
	for idx, oldmreq := range mreqs.Unfiltered {
		if oldmreq.Equal(mreq) {
			mreqs.Unfiltered[idx] = mreq
			found = true
			log.Info("Updated existing mreq")
			break
		}
	}
	if !found {
		log.Info("Got new mreq")
		mreqs.Unfiltered = append(mreqs.Unfiltered, mreq)
	}

	mreqs.Refresh()
}

func (mreqs *MergeReqList) PurgeRepo(repo *Repo) {
	newmreqs := []*MergeReq{}
	for _, mreq := range mreqs.Unfiltered {
		if !mreq.Repo.Equal(repo) {
			newmreqs = append(newmreqs, mreq)
		}
	}
	mreqs.Unfiltered = newmreqs
	mreqs.Refresh()
}

func (mreqs *MergeReqList) Len() int {
	return len(mreqs.Active)
}

func (mreqs *MergeReqList) Less(i, j int) bool {
	return mreqs.Sorter(*mreqs.Active[i], *mreqs.Active[j])
}

func (mreqs *MergeReqList) Swap(i, j int) {
	mreq := mreqs.Active[i]
	mreqs.Active[i] = mreqs.Active[j]
	mreqs.Active[j] = mreq
}

func (mreqs *MergeReqList) Refresh() {
	var active []*MergeReq
	if mreqs.Filter == nil {
		active = mreqs.Unfiltered
	} else {
		for _, mreq := range mreqs.Unfiltered {
			if mreqs.Filter(*mreq) {
				active = append(active, mreq)
			}
		}
	}

	mreqs.Active = active

	if mreqs.Sorter != nil {
		sort.Sort(mreqs)
	}
}
