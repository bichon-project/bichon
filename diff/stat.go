// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2020 Red Hat, Inc.

package diff

import (
	"fmt"
	"sort"
	"strings"

	log "github.com/sirupsen/logrus"
)

type FileDiffStat struct {
	Name    string
	Added   int
	Removed int
	Binary  bool
}

func DiffStat(diffs map[string]string) ([]FileDiffStat, error) {
	var stats []FileDiffStat
	for file, content := range diffs {
		stat := FileDiffStat{
			Name: file,
		}

		if IsBinaryDiffSummary(content) {
			stat.Binary = true
		} else {
			hunks, err := ParseUnifiedDiffHunks(content)
			if err != nil {
				return []FileDiffStat{}, err
			}

			for _, hunk := range hunks {
				for _, line := range hunk.Lines {
					if line.Type == DIFF_LINE_ADDED {
						stat.Added++
					} else if line.Type == DIFF_LINE_REMOVED {
						stat.Removed++
					}
				}
			}
		}
		stats = append(stats, stat)
	}

	return stats, nil
}

func CommonPrefix(lines []string) string {
	if len(lines) == 0 {
		return ""
	}
	shortest := 0
	for idx, line := range lines {
		if len(line) < len(lines[shortest]) {
			shortest = idx
		}
	}

	prefix := ""
	for i := 0; i < len(lines[shortest]); i++ {
		newprefix := prefix + lines[shortest][i:i+1]
		for _, line := range lines {
			if !strings.HasPrefix(line, newprefix) {
				return prefix
			}
		}
		prefix = newprefix
	}
	return prefix
}

func DiffStatSummary(stats []FileDiffStat) string {
	added := 0
	removed := 0
	for _, stat := range stats {
		added += stat.Added
		removed += stat.Removed
	}

	return fmt.Sprintf(" %d files changed, %d insertions(+), %d deletions(-)",
		len(stats), added, removed)
}

func FormatDiffStat(stats []FileDiffStat, width int) []string {
	if width < 40 {
		return []string{DiffStatSummary(stats)}
	}

	binary := "binary file"

	var names []string
	statsMap := make(map[string]*FileDiffStat)
	mostChanged := 0
	for idx, _ := range stats {
		stat := &stats[idx]
		names = append(names, stat.Name)
		statsMap[stat.Name] = stat
		var changed int
		if stat.Binary {
			changed = len(binary)
		} else {
			changed = stat.Added + stat.Removed
		}
		if changed > mostChanged {
			mostChanged = changed
		}
	}
	sort.Strings(names)

	if mostChanged%2 == 1 {
		mostChanged++
	}

	trim := CommonPrefix(names)
	trimlen := len(trim)
	maxnamewidth := 0
	for _, name := range names {
		//shortname := name[trimlen:]
		shortname := name
		if len(shortname) > maxnamewidth {
			maxnamewidth = len(shortname)
		}
	}

	statwidth := 20
	if mostChanged < statwidth {
		statwidth = mostChanged
	}

	namewidth := width - statwidth - 3

	if maxnamewidth > namewidth {
		maxnamewidth = namewidth
	} else if maxnamewidth < namewidth {
		namewidth = maxnamewidth
		statwidth = width - namewidth - 3
	}
	var changeFactor int
	if mostChanged < statwidth {
		changeFactor = 1
	} else {
		changeFactor = (mostChanged / statwidth) + 2
	}
	log.Infof("most changed %d width %d factor %d", mostChanged, statwidth, changeFactor)

	var info []string
	for _, name := range names {
		stat := statsMap[name]

		shortname := name[trimlen:]
		shortname = name

		added := stat.Added / changeFactor
		removed := stat.Removed / changeFactor
		log.Infof("%s added %d %d removed %d %d", name, added, stat.Added, removed, stat.Removed)

		var summary string
		if stat.Binary {
			fmtstr := " %-" + fmt.Sprintf("%d", namewidth) + "s | %s"
			summary = fmt.Sprintf(fmtstr, shortname, binary)
		} else {
			fmtstr := " %-" + fmt.Sprintf("%d", namewidth) + "s | %s%s"
			summary = fmt.Sprintf(fmtstr,
				shortname, strings.Repeat("-", removed), strings.Repeat("+", added))
		}
		info = append(info, summary)
	}

	info = append(info, DiffStatSummary(stats))
	return info
}
