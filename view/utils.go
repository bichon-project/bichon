// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package view

import (
	"unicode"

	runewidth "github.com/mattn/go-runewidth"
	"github.com/rivo/uniseg"
	"gitlab.com/bichon-project/tview"
)

func stringWidth(text string) (width int) {
	g := uniseg.NewGraphemes(text)
	for g.Next() {
		var chWidth int
		for _, r := range g.Runes() {
			chWidth = runewidth.RuneWidth(r)
			if chWidth > 0 {
				break // Our best guess at this point is to use the width of the first non-zero-width rune.
			}
		}
		width += chWidth
	}
	return
}

func TrimEllipsisFront(data string, maxlen int) string {
	graphemes := [][]rune{}
	widths := []int{}

	g := uniseg.NewGraphemes(data)
	for g.Next() {
		var chWidth int
		// Find width of the first non-zero width rune.
		// Assume other runes won't affect this width
		// when combined on screen
		for _, r := range g.Runes() {
			chWidth = runewidth.RuneWidth(r)
			if chWidth > 0 {
				break
			}
		}
		graphemes = append(graphemes, g.Runes())
		widths = append(widths, chWidth)
	}

	total := 0
	want := []rune{}
	truncated := false
	for i := len(widths) - 1; i >= 0; i-- {
		width := widths[i]
		if total+width < maxlen {
			want = append(graphemes[i], want...)
			total += width
		} else if total+width == maxlen {
			if i == 0 {
				want = append(graphemes[i], want...)
				total += width
			} else {
				truncated = true
				break
			}
		} else {
			truncated = true
			break
		}
	}

	if truncated {
		offset := 0
		for i, r := range want {
			if !unicode.IsSpace(r) {
				break
			}
			offset = i + 1
		}
		return "…" + string(want[offset:])
	} else {
		return data
	}
}

func HighlightTrailingSpace(text, style string, offset uint) string {
	firstspace := -1
	for idx, c := range text {
		if uint(idx) >= offset && unicode.Is(unicode.White_Space, c) {
			if firstspace == -1 {
				firstspace = idx
			}
		} else {
			firstspace = -1
		}
	}
	if firstspace != -1 {
		text = text[0:firstspace] + style + text[firstspace:]
	}
	return text
}

func Modal(p tview.Primitive, width, height int) tview.Primitive {
	return tview.NewFlex().
		AddItem(nil, 0, 1, false).
		AddItem(tview.NewFlex().SetDirection(tview.FlexRow).
			AddItem(nil, 0, 1, false).
			AddItem(p, height, 1, true).
			AddItem(nil, 0, 1, false), width, 1, true).
		AddItem(nil, 0, 1, false)
}
