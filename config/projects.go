// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package config

import (
	"fmt"
	"os"

	"github.com/go-ini/ini"
	log "github.com/sirupsen/logrus"

	"gitlab.com/bichon-project/bichon/config/xdg"
	"gitlab.com/bichon-project/bichon/model"
	"gitlab.com/bichon-project/bichon/security"
)

func SaveProjects(repos []model.Repo, tokenKey [32]byte) error {
	projectspath := xdg.ConfigPath("projects.ini")

	cfg := ini.Empty()

	for _, repo := range repos {
		token, err := security.EncryptToken(repo.Token, tokenKey)
		if err != nil {
			return err
		}

		secname := fmt.Sprintf("%s:%s", repo.Directory, repo.Remote)

		sec, _ := cfg.NewSection(secname)

		sec.NewKey("nickname", repo.NickName)
		sec.NewKey("directory", repo.Directory)
		sec.NewKey("remote", repo.Remote)
		sec.NewKey("server", repo.Server)
		sec.NewKey("project", repo.Project)
		key, _ := sec.NewKey("token", token)
		key.Comment = "Not the raw API token, base64 ciphertext after encrypting token with a secret key"
		if repo.GlobalToken {
			sec.NewKey("global-token", "true")
		} else {
			sec.NewKey("global-token", "false")
		}
		sec.NewKey("state", string(repo.State))
	}

	return AtomicSave(cfg, projectspath)
}

func LoadProjects(tokenKey [32]byte, compatToken bool) ([]model.Repo, error) {
	projectspath := xdg.ConfigPath("projects.ini")

	cfg, err := ini.Load(projectspath)
	if err != nil {
		if os.IsNotExist(err) {
			return []model.Repo{}, nil
		}
		return []model.Repo{}, err
	}

	log.Infof("Loading repos from %s", projectspath)
	var repos []model.Repo
	for _, sec := range cfg.Sections() {
		if sec.Name() == "DEFAULT" {
			continue
		}

		// Old configs used section name as the key
		// but now section name is dir:remote, so we
		// must use separate key if it exists
		dir := sec.Name()
		if sec.HasKey("directory") {
			dir = sec.Key("directory").String()
		}

		remote := sec.Key("remote").String()
		server := sec.Key("server").String()
		project := sec.Key("project").String()

		var nickname string
		if sec.HasKey("nickname") {
			nickname = sec.Key("nickname").String()
		}

		var token string
		var globalToken bool
		if compatToken {
			token, globalToken, err = security.FetchToken(server, project)
			if err != nil {
				return []model.Repo{}, err
			}
		} else {
			if !sec.HasKey("token") {
				return []model.Repo{}, fmt.Errorf("Missing 'token' in projects.ini for '%s'", project)
			}
			enctoken := sec.Key("token").String()
			globalToken = sec.Key("global-token").MustBool()

			token, err = security.DecryptToken(enctoken, tokenKey)
			if err != nil {
				return []model.Repo{}, err
			}
		}

		state := model.RepoStateActive
		if sec.HasKey("state") {
			state = model.RepoState(sec.Key("state").String())
		}

		repo := model.NewRepo(nickname, dir, remote, server, project, token, globalToken, state)

		repos = append(repos, *repo)
	}

	return repos, nil
}
